﻿using RespService.DataTypes;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RespService
{
	public abstract class RespConnection : IDisposable
	{
		private Socket _socket;
		private RespProtocolStream _protocolStream;
		private NetworkStream _stream;
		private int _bufferSize;

		public RespConnection(Socket socket)
			: this()
		{
			AssignSocket(socket);
		}

		public RespConnection()
		{
			_bufferSize = 2048;
			this.Encoding = Encoding.UTF8;
		}

		~RespConnection()
		{
			Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
		}

		protected virtual void Dispose(bool disposing)
		{
			ReleaseSocket();
		}

		protected bool SocketAssigned
		{
			get { return _socket != null; }
		}

		protected void AssignSocket(Socket socket)
		{
			if (_socket != null)
				throw new InvalidOperationException();
			if (socket == null)
				throw new ArgumentNullException(nameof(socket));
			ConfigureSocket(socket);
			_socket = socket;
			_protocolStream = new RespProtocolStream();
		}

		protected Socket ReleaseSocket()
		{
			Socket socket = Interlocked.Exchange(ref _socket, null); 
			if (socket != null)
			{
				if (socket.Connected)
				{
					socket.Shutdown(SocketShutdown.Both);
				}
				socket.Dispose();
			}

			NetworkStream stream = Interlocked.Exchange(ref _stream, null);
			if (stream != null)
			{
				stream.Dispose();
			}

			RespProtocolStream protocolStream = Interlocked.Exchange(ref _protocolStream, null);
			if (protocolStream != null)
			{
				protocolStream.Dispose();
			}

			return socket;
		}

		protected abstract void ThrowInassignedSockedException();

		protected virtual void ConfigureSocket(Socket socket)
		{

		}

		protected virtual Socket Socket
		{
			get
			{
				Socket socket = _socket;
				if (socket == null)
					ThrowInassignedSockedException();
				return socket;
			}
		}

		protected NetworkStream Stream
		{
			get
			{
				if (_stream == null)
					_stream = new NetworkStream(this.Socket);
				return _stream;
			}
		}

		public Encoding Encoding { get; set; }

		public int BufferSize
		{
			get
			{
				return _bufferSize;
			}
			set
			{
				if (value <= 0)
					throw new ArgumentOutOfRangeException(nameof(value));
				_bufferSize = value;
			}
		}

		public virtual bool Connected
		{
			get
			{
				Socket socket = _socket;
				return socket != null && socket.Connected;
			}
		}

		private RespProtocolStream StatementStream
		{
			get { return _protocolStream; }
		}

		protected abstract CancellationToken ShutdownToken { get; }

		protected abstract Task ShutdownTask { get; }

		private void AssertConnection()
		{
			if (this.Socket.Poll(0, SelectMode.SelectError))
				throw new IOException("Connection closed!");
		}

		protected virtual Task ProcessExceptionAsync(RespException exception)
		{
#if CORECLR
			return Task.CompletedTask;
#else
			return Task.Delay(0);
#endif
		}

		protected virtual void ProcessException(RespException exception)
		{

		}

		protected async Task WriteAsync(byte[] buffer, int offset, int count)
		{
			await this.Stream.WriteAsync(buffer, offset, count).ConfigureAwait(false);
			await this.Stream.FlushAsync().ConfigureAwait(false);
		}

		protected void Write(byte[] buffer, int offset, int count)
		{
			this.Stream.Write(buffer, offset, count);
			this.Stream.Flush();
		}

		protected async Task WriteStatementAsync(RespData statement)
		{
			ArraySegment<byte> segment = statement.GetBuffer();
			await this.Stream.WriteAsync(segment.Array, segment.Offset, segment.Count, this.ShutdownToken).ConfigureAwait(false);
			await this.Stream.FlushAsync().ConfigureAwait(false);
		}

		protected void WriteStatement(RespData statement)
		{
			ArraySegment<byte> segment = statement.GetBuffer();
			this.Stream.Write(segment.Array, segment.Offset, segment.Count);
			this.Stream.Flush();
		}

		protected async Task<RespData> ReadStatementAsync()
		{
			RespData statement;
			byte[] buffer = ArrayPool<byte>.Shared.Rent(this.BufferSize);
			try
			{
				while (true)
				{
					while (this.StatementStream.CanReadStatement())
					{
						Task<int> readTask = this.Stream.ReadAsync(buffer, 0, buffer.Length, this.ShutdownToken);
						int count = await readTask.ConfigureAwait(false);
						if (count == 0 || !readTask.IsCompleted)
						{
							if (readTask.Exception != null)
								throw readTask.Exception;
							AssertConnection();
							throw new IOException("Connection closed!");
						}
						this.StatementStream.Seek(0, SeekOrigin.End);
						this.StatementStream.Write(buffer, 0, count);
					}

					try
					{
						statement = RespData.ParseStream(this.StatementStream, 0);
					}
					catch (RespProtocolException respEx)
					{
						await ProcessExceptionAsync(respEx).ConfigureAwait(false);
						throw;
					}

					if (statement.DataType != RespDataType.Undefined)
						break;

					this.StatementStream.StatementConditionOffset = this.StatementStream.Position;
				}
			}
			finally
			{
				ArrayPool<byte>.Shared.Return(buffer);
			}

			VacuumStatementStream();

			return statement;
		}

		protected RespData ReadStatement()
		{
			RespData statement;
			byte[] buffer = ArrayPool<byte>.Shared.Rent(this.BufferSize);
			try
			{
				while (true)
				{
					while (this.StatementStream.CanReadStatement())
					{
						int count = this.Stream.Read(buffer, 0, buffer.Length);
						if (count == 0)
						{
							throw new IOException("Connection closed!");
						}
						this.StatementStream.Seek(0, SeekOrigin.End);
						this.StatementStream.Write(buffer, 0, count);
					}

					try
					{
						statement = RespData.ParseStream(this.StatementStream, 0);
					}
					catch (RespProtocolException respEx)
					{
						ProcessException(respEx);
						throw;
					}

					if (statement.DataType != RespDataType.Undefined)
						break;

					this.StatementStream.StatementConditionOffset = this.StatementStream.Position;
				}
			}
			finally
			{
				ArrayPool<byte>.Shared.Return(buffer);
			}

			VacuumStatementStream();

			return statement;
		}

		private void VacuumStatementStream()
		{
			var stream = new RespProtocolStream();
			this.StatementStream.CopyTo(stream);
			_protocolStream = stream;
		}

		protected static void EnableKeepAlive(Socket socket)
		{
			if (socket == null)
				throw new ArgumentOutOfRangeException(nameof(socket));

#if CORECLR
			if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
#else
			if (!Utils.IsMono)
#endif
			{
				var tcpkeepalive = new TcpKeepAlive();
				tcpkeepalive.onoff = 1; // enable
				tcpkeepalive.keepalivetime = 10000; // send a packet once every 10 seconds
				tcpkeepalive.keepaliveinterval = 1000; // if no response, resend every second 
				socket.IOControl(IOControlCode.KeepAliveValues, tcpkeepalive.AsBytes(), null);
			}
			else
			{
				// TCP_KEEPIDLE, TCP_KEEPINTVL

			}
		}

	}
}
