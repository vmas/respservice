﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace RespService
{
	[StructLayout(LayoutKind.Explicit)]
	unsafe struct TcpKeepAlive
	{
		[FieldOffset(0)]
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
		private fixed byte _bytes[12];

		[FieldOffset(0)]
		public uint onoff;

		[FieldOffset(4)]
		public uint keepalivetime;

		[FieldOffset(8)]
		public uint keepaliveinterval;

		public unsafe byte[] AsBytes()
		{
			byte[] array = new byte[12];
			fixed (void* ptr = _bytes)
			{
				Marshal.Copy(new IntPtr(ptr), array, 0, array.Length);
			}
			return array;
		}
	}
}
