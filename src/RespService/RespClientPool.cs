﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RespService
{
	public class RespClientPool : IDisposable
	{
		private static Timer _PruningTimer = new Timer(PruneIdleClients, new object(), System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
		private static List<WeakReference<RespClientPool>> _Pools = new List<WeakReference<RespClientPool>>();

		private readonly IRespClientFactory _clientFactory;
		private readonly object _verificationToken;
		private IRespClientInternal[] _clients;
		private readonly SyncHandle _clientReturnedEvent;
		private TimeSpan _timeout;
		private readonly SyncHandle _syncRoot;
		private readonly RespClientPoolPerformance _performance;
		private int _minPoolSize;
		private TimeSpan _maxIdleTime;
		private WeakReference<RespClientPool> _weakRef;

		public RespClientPool(int minPoolSize, int maxPoolSize, IRespClientFactory clientFactory)
			: this(maxPoolSize, clientFactory)
		{
			this.MinPoolSize = minPoolSize;
		}

		public RespClientPool(int poolSize, IRespClientFactory clientFactory)
		{
			if (poolSize <= 0)
				throw new ArgumentOutOfRangeException(nameof(poolSize));
			if (clientFactory == null)
				throw new ArgumentNullException(nameof(clientFactory));

			_verificationToken = new object();
			_performance = new RespClientPoolPerformance(this);
			_syncRoot = new SyncHandle(1, 1);
			_clientReturnedEvent = new SyncHandle(poolSize);
			_clients = new IRespClientInternal[poolSize];
			_clientFactory = clientFactory;
			this.Timeout = 30000;
			_maxIdleTime = TimeSpan.FromSeconds(120);
			_minPoolSize = Math.Min(Math.Max(poolSize / 5, 4), poolSize);
			EnablePruning();
		}

		~RespClientPool()
		{
			Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			IRespClientInternal[] clients = Interlocked.Exchange(ref _clients, null);
			if (clients == null)
				return;

			DisablePruning();

			_clientReturnedEvent.Dispose();
			_syncRoot.Dispose();
			for (int i = 0; i < clients.Length; i++)
			{
				IRespClientInternal client = Interlocked.Exchange(ref clients[i], null);
				if (client != null)
				{
					client.Dispose();
				}
			}
		}


		private static void PruneIdleClients(object pruningSyncRoot)
		{
			if (!Monitor.TryEnter(pruningSyncRoot))
				return;

			try
			{
				WeakReference<RespClientPool>[] poolRefs;
				lock (_Pools)
				{
					poolRefs = _Pools.ToArray();
				}

				foreach (WeakReference<RespClientPool> weakRef in poolRefs)
				{
					if (!weakRef.TryGetTarget(out RespClientPool pool))
						continue;

					try
					{
						int minPoolSize = pool.MinPoolSize;
						if (pool.Available <= 0 || minPoolSize >= pool.PoolSize - pool.Performance.UnusedSlots)
							continue;

						IRespClientInternal[] clients = pool.Clients;
						for (int i = clients.Length - 1; i >= minPoolSize; i--)
						{
							IRespClientInternal client = Volatile.Read(ref clients[i]);
							if (client == null)
								continue;
							if (client.Available && client.GetIdleTime() > pool.MaxIdleTime)
							{
								if (!client.TryMakeUnavailableThreadSafe(pool._verificationToken))
									continue;

								Interlocked.CompareExchange(ref clients[i], null, client);
								client.MoveToPool(null);
								client.Dispose();
							}
						}
					}
					catch { }
				}
			}
			finally
			{
				Monitor.Exit(pruningSyncRoot);
			}
		}

		protected void EnablePruning()
		{
			var weakRef = new WeakReference<RespClientPool>(this);
			if (Interlocked.CompareExchange(ref _weakRef, weakRef, null) != null)
				return;

			lock (_Pools)
			{
				_Pools.Add(weakRef);
				_PruningTimer.Change(60000, 60000);
			}
		}

		protected void DisablePruning()
		{
			WeakReference<RespClientPool> weakRef = Interlocked.Exchange(ref _weakRef, null);
			if (weakRef == null)
				return;

			lock (_Pools)
			{
				_Pools.Remove(weakRef);
				if (_Pools.Count == 0)
				{
					_PruningTimer.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
				}
			}
		}

		public TimeSpan MaxIdleTime
		{
			get
			{
				return _maxIdleTime;
			}
			set
			{
				if (value < TimeSpan.Zero)
					throw new ArgumentOutOfRangeException(nameof(value));
				_maxIdleTime = value;
			}
		}

		public int MinPoolSize
		{
			get
			{
				return _minPoolSize;
			}
			set
			{
				if (_minPoolSize < 1 || _minPoolSize > this.PoolSize)
					throw new ArgumentOutOfRangeException(nameof(value));
				_minPoolSize = value;
			}
		}

		public int PoolSize
		{
			get { return this.Clients.Length; }
		}

		public int Available
		{
			get { return _clientReturnedEvent.CurrentCount; }
		}

		public RespClientPoolPerformance Performance
		{
			get { return _performance; }
		}

		protected IRespClientFactory ClientFactory
		{
			get { return _clientFactory; }
		}

		internal IRespClientInternal[] Clients
		{
			get
			{
				IRespClientInternal[] clients = _clients;
				if (clients != null)
					return clients;
				throw new ObjectDisposedException(this.GetType().Name);
			}
		}

		public int Timeout
		{
			get
			{
				return (int)_timeout.TotalMilliseconds;
			}
			set
			{
				if (value == System.Threading.Timeout.Infinite)
				{
					_timeout = System.Threading.Timeout.InfiniteTimeSpan;
				}
				else
				{
					if (value <= 0)
						throw new ArgumentOutOfRangeException();
					_timeout = TimeSpan.FromMilliseconds(value);
				}
			}
		}

		private async Task<IRespClientInternal> GetOrCreateClientInternalAsync(bool syncMode, TimeSpan timeout, CancellationToken cancellationToken)
		{
			IRespClientInternal client;
			IRespClientInternal[] clients = this.Clients;

			for (int i = 0; i < clients.Length; i++)
			{
				client = Volatile.Read(ref clients[i]);
				if (client != null)
				{
					if (client.TryMakeUnavailableThreadSafe(_verificationToken))
					{
						if (!client.Connected)
						{
							Volatile.Write(ref clients[i], null);
							client.Dispose();
							continue;
						}
						return client;
					}
				}
			}

			Interlocked.Increment(ref _performance._waiting);
			try
			{
				if (syncMode)
				{
					if (!_syncRoot.Wait(timeout, cancellationToken))
						throw new TimeoutException(string.Format("Timeout while getting or creating a client for the {0} pool.", this.GetType().Name));
				}
				else
				{
					if (!await _syncRoot.WaitAsync(timeout, cancellationToken).ConfigureAwait(false))
						throw new TimeoutException(string.Format("Timeout while getting or creating a client for the {0} pool.", this.GetType().Name));
				}
				try
				{
					int emptySlotIndex = -1;
					do
					{
						for (int i = 0; i < clients.Length; i++)
						{
							client = Volatile.Read(ref clients[i]);
							if (client != null)
							{
								if (client.TryMakeUnavailableThreadSafe(_verificationToken))
								{
									if (!client.Connected)
									{
										Volatile.Write(ref clients[i], null);
										client.Dispose();
										continue;
									}
									return client;
								}
								continue;
							}
							if (emptySlotIndex != -1)
								continue;
							emptySlotIndex = i;
						}
					} while (emptySlotIndex == -1);

					client = this.ClientFactory.CreateClient();
					if (client == null)
						throw new InvalidOperationException();
					client.MoveToPool(this);
					client.MakeUnavailable(_verificationToken);
					Volatile.Write(ref clients[emptySlotIndex], client);
				}
				finally
				{
					_syncRoot.Release();
				}
			}
			finally
			{
				Interlocked.Decrement(ref _performance._waiting);
			}
			return client;
		}

		public virtual IRespClient GetClient()
		{
			return GetClient(_timeout, CancellationToken.None);
		}

		public virtual IRespClient GetClient(TimeSpan timeout, CancellationToken cancellationToken)
		{
			Interlocked.Increment(ref _performance._starvation);
			try
			{
				if (!_clientReturnedEvent.Wait(timeout, cancellationToken))
					throw new TimeoutException(string.Format("The connection pool {0} has been exhausted.", this.GetType().Name));
			}
			finally
			{
				Interlocked.Decrement(ref _performance._starvation);
			}

			IRespClientInternal client;
			try
			{
				cancellationToken.ThrowIfCancellationRequested();
				Task<IRespClientInternal> clientCreationTask = GetOrCreateClientInternalAsync(true, timeout, cancellationToken);
				client = clientCreationTask.GetAwaiter().GetResult();
			}
			catch
			{
				_clientReturnedEvent.Release();
				throw;
			}
			if (!client.Connected)
			{
				Interlocked.Increment(ref _performance._connecting);
				try
				{
					client.Connect();
				}
				catch
				{
					client.Dispose();
					throw;
				}
				finally
				{
					Interlocked.Decrement(ref _performance._connecting);
				}
			}
			return client;
		}

		public virtual Task<IRespClient> GetClientAsync()
		{
			return GetClientAsync(_timeout, CancellationToken.None);
		}

		public virtual async Task<IRespClient> GetClientAsync(TimeSpan timeout, CancellationToken cancellationToken)
		{
			Interlocked.Increment(ref _performance._starvation);
			try
			{
				if (!await _clientReturnedEvent.WaitAsync(timeout, cancellationToken).ConfigureAwait(false))
					throw new TimeoutException(string.Format("The connection pool {0} has been exhausted.", this.GetType().Name));
			}
			finally
			{
				Interlocked.Decrement(ref _performance._starvation);
			}

			IRespClientInternal client;
			try
			{
				cancellationToken.ThrowIfCancellationRequested();
				client = await GetOrCreateClientInternalAsync(false, timeout, cancellationToken).ConfigureAwait(false);
			}
			catch
			{
				_clientReturnedEvent.Release();
				throw;
			}
			if (!client.Connected)
			{
				Interlocked.Increment(ref _performance._connecting);
				try
				{
					await client.ConnectAsync().ConfigureAwait(false);
				}
				catch
				{
					client.Dispose();
					throw;
				}
				finally
				{
					Interlocked.Decrement(ref _performance._connecting);
				}
			}
			return client;
		}

		public virtual bool RevokeClient(IRespClientInternal client)
		{
			if (client == null)
				throw new ArgumentNullException(nameof(client));

			int index = GetSlotIndex(client);
			if (index == -1)
				return false;

			if (client.Connected)
			{
				client.MakeAvailable(_verificationToken);
				try
				{
					_clientReturnedEvent.Release();
				}
				catch (ObjectDisposedException) { }
				return true;
			}

			FreeSlotUnsafe(client, index);
			return false;
		}

		public virtual bool RemoveFromPool(IRespClientInternal client)
		{
			if (client == null)
				throw new ArgumentNullException(nameof(client));

			int index = GetSlotIndex(client);
			if (index == -1)
				return false;

			FreeSlotUnsafe(client, index);
			return true;
		}

		private void FreeSlotUnsafe(IRespClientInternal client, int slot)
		{
			IRespClientInternal[] clients = Volatile.Read(ref _clients);
			if (clients != null)
			{
				if (Interlocked.CompareExchange(ref clients[slot], null, client) == client)
				{
					try
					{
						_clientReturnedEvent.Release();
					}
					catch (ObjectDisposedException) { }
				}
				if (client.Pool == this)
				{
					client.MoveToPool(null);
				}
			}
		}

		private int GetSlotIndex(IRespClientInternal client)
		{
			IRespClientInternal[] clients = Volatile.Read(ref _clients);
			if (client != null && clients != null)
			{
				for (int i = 0; i < clients.Length; i++)
				{
					if (Volatile.Read(ref clients[i]) == client)
						return i;
				}
			}
			return -1;
		}

	}
}
