﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;

namespace RespService
{
	sealed class RespProtocolStream : MemoryStream
	{
		public RespProtocolStream()
		{
			this.CanReadStatement = InlineStatementReaded;
		}

		public RespProtocolStream(byte[] buffer, int offset, int count)
			: base(buffer, offset, count, false, true)
		{
			this.CanReadStatement = InlineStatementReaded;
		}

		public Func<bool> CanReadStatement { get; private set; }

		public long StatementConditionOffset { get; set; }

		private bool InlineStatementReaded()
		{
#if CORECLR
			ArraySegment<byte> segment;
			if (!this.TryGetBuffer(out segment))
				return true;
#else
			IEnumerable<byte> segment = this.GetBuffer().Take((int)this.Position);
#endif
			long offset = 0;
			long conditionOffset = this.StatementConditionOffset;
			byte prevByte = 0;
			foreach (byte b in segment)
			{
				if (offset >= conditionOffset)
				{
					if (b == (byte)'\n' && prevByte == (byte)'\r')
					{
						return false;
					}
				}
				offset++;
				prevByte = b;
			}
			return true;
		}

	}
}
