﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

namespace RespService.DataTypes
{
	[StructLayout(LayoutKind.Explicit)]
	public struct RespArray : IEnumerable<RespData>
	{
		[FieldOffset(0)]
		private readonly int _offset;
		[FieldOffset(4)]
		private readonly int _count;
		[FieldOffset(8)]
		private readonly byte[] _buffer;

		public RespArray(params RespData[] data)
		{
			_offset = 0;
			int length = data != null ? data.Length : -1;
			byte[] arrayInfoBuffer = Encoding.ASCII.GetBytes("*" + length.ToString(NumberFormatInfo.InvariantInfo) + "\r\n");
			if (length != -1)
			{
				ArraySegment<byte> dataItem;
				var buffers = new ArraySegment<byte>[length];
				length = 0;
				for (int i = 0; i < buffers.Length; i++)
				{
					dataItem = data[i].GetBuffer();
					buffers[i] = dataItem;
					length += dataItem.Count;
				}
				byte[] buffer  = new byte[length + arrayInfoBuffer.Length];
				Buffer.BlockCopy(arrayInfoBuffer, 0, buffer, 0, arrayInfoBuffer.Length);
				length = arrayInfoBuffer.Length;
				for (int i = 0; i < buffers.Length; i++)
				{
					dataItem = buffers[i];
					Buffer.BlockCopy(dataItem.Array, dataItem.Offset, buffer, length, dataItem.Count);
					length += dataItem.Count;
				}			  
				_count = buffer.Length;
				_buffer = buffer;
			}
			else
			{				 
				_count = arrayInfoBuffer.Length;
				_buffer = arrayInfoBuffer;
			}
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private RespArray(byte[] buffer, int offset, int count)
		{
			_offset = offset;
			_count = count;
			_buffer = buffer;
		}

		public bool IsNull()
		{
			return GetLength() == -1;
		}

		public int GetLength()
		{
			AssertValidBuffer();
			int end = Array.IndexOf<byte>(_buffer, (byte)'\r', _offset);
			return int.Parse(Encoding.ASCII.GetString(_buffer, _offset + 1, end - _offset - 1), NumberFormatInfo.InvariantInfo);
		}

		public IEnumerator<RespData> GetEnumerator()
		{
			AssertValidBuffer();
			return new RespArrayEnumerator(this);
		}

		private void AssertValidBuffer()
		{
			if (_buffer == null || _buffer.Length <= 3 || _buffer.Length < _offset + _count || _buffer[_offset] != '*')
				throw new InvalidOperationException();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static implicit operator RespData(RespArray multibulk)
		{
			return new RespData(multibulk._buffer, multibulk._offset, multibulk._count);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static explicit operator RespArray(RespData data)
		{
			return new RespArray(data._buffer, data._offset, data._count);
		}

		private class RespArrayEnumerator : IEnumerator<RespData>
		{
			private readonly RespArray _array;
			private RespData _current;
			private int _offset;
			private RespProtocolStream _stream;

			internal RespArrayEnumerator(RespArray array)
			{
				_array = array;
				_stream = new RespProtocolStream(array._buffer, array._offset, array._count);
			}

			public RespData Current
			{
				get { return _current; }
			}

			object IEnumerator.Current
			{
				get { return _current; }
			}

			void IDisposable.Dispose()
			{
				_stream.Dispose();
			}

			public bool MoveNext()
			{
				ArraySegment<byte> segment;
				if (_offset == 0)
				{
					if (!RespData.TryReadToLineEnd(_stream, 0, out segment))
						throw new InvalidOperationException();
					_offset = segment.Count;
				}

				if (_offset >= _array._count)
					return false;

				segment = RespData.ParseData(_stream, _offset);
				_offset += segment.Count;
				_current = new RespData(segment.Array, segment.Offset, segment.Count);
				return true;
			}

			public void Reset()
			{
				_offset = 0;
				_current = RespData.None;
				_stream.Seek(0, SeekOrigin.Begin);
			}
		}
	}
}
