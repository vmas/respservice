﻿using System;
using System.Buffers;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

namespace RespService.DataTypes
{
	[StructLayout(LayoutKind.Explicit)]
	public struct RespString
	{
		[FieldOffset(0)]
		private readonly int _offset;
		[FieldOffset(4)]
		private readonly int _count;
		[FieldOffset(8)]
		private readonly byte[] _buffer;

		public RespString(string s, Encoding encoding) 
		{
			byte[] buffer = ArrayPool<byte>.Shared.Rent(encoding.GetMaxByteCount(s.Length));
			try
			{
				int count = encoding.GetBytes(s, 0, s.Length, buffer, 0);
				byte[] raw = new byte[count + 3];
				raw[0] = (byte)'+';
				raw[raw.Length - 2] = (byte)'\r';
				raw[raw.Length - 1] = (byte)'\n';
				Buffer.BlockCopy(buffer, 0, raw, 1, count);
				_offset = 0;
				_buffer = raw;
				_count = raw.Length;
			}
			finally
			{
				ArrayPool<byte>.Shared.Return(buffer);
			}
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private RespString(byte[] buffer, int offset, int count)
		{
			_offset = offset;
			_count = count;
			_buffer = buffer;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static implicit operator RespData(RespString s)
		{
			return new RespData(s._buffer, s._offset, s._count);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static explicit operator RespString(RespData data)
		{
			return new RespString(data._buffer, data._offset, data._count);
		}

		public string GetString(Encoding encoding)
		{
			if (_buffer == null || _count < 2)
				throw new InvalidOperationException();
			if (_buffer[_offset] == '+')
			{
				if (_count < 3)
					throw new InvalidOperationException();
				return encoding.GetString(_buffer, _offset + 1, _count - 3);
			}
			return encoding.GetString(_buffer, _offset, _count - 2);
		}

	}
}
