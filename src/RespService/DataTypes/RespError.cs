﻿using System;
using System.Buffers;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

namespace RespService.DataTypes
{
	[StructLayout(LayoutKind.Explicit)]
	public struct RespError
	{
		[FieldOffset(0)]
		private readonly int _offset;
		[FieldOffset(4)]
		private readonly int _count;
		[FieldOffset(8)]
		private readonly byte[] _buffer;

		public RespError(string s, Encoding encoding)
		{
			byte[] buffer = ArrayPool<byte>.Shared.Rent(encoding.GetMaxByteCount(s.Length));
			try
			{
				int count = encoding.GetBytes(s, 0, s.Length, buffer, 0);
				byte[] data = new byte[count + 3];
				data[0] = (byte)'-';
				data[data.Length - 2] = (byte)'\r';
				data[data.Length - 1] = (byte)'\n';
				Buffer.BlockCopy(buffer, 0, data, 1, count);
				_offset = 0;
				_count = data.Length;
				_buffer = data;
			}
			finally
			{
				ArrayPool<byte>.Shared.Return(buffer);
			}
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private RespError(byte[] buffer, int offset, int count)
		{
			_offset = offset;
			_count = count;
			_buffer = buffer;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static implicit operator RespData(RespError error)
		{
			return new RespData(error._buffer, error._offset, error._count);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static explicit operator RespError(RespData data)
		{
			return new RespError(data._buffer, data._offset, data._count);
		}

		public string GetString(Encoding encoding)
		{
			if (_buffer == null || _count < 3 || _buffer[_offset] != '-')
				throw new InvalidOperationException();
			return encoding.GetString(_buffer, _offset + 1, _count - 3);
		}
	}
}
