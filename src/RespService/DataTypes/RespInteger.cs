﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

namespace RespService.DataTypes
{
	[StructLayout(LayoutKind.Explicit)]
	public sealed class RespInteger
	{
		[FieldOffset(0)]
		private readonly int _offset;
		[FieldOffset(4)]
		private readonly int _count;
		[FieldOffset(8)]
		private readonly byte[] _buffer;

		public RespInteger(long value)
		{
			string s = value.ToString(NumberFormatInfo.InvariantInfo);
			byte[] data = new byte[s.Length + 3];
			if (Encoding.ASCII.GetBytes(s, 0, s.Length, data, 1) != s.Length)
				throw new InvalidOperationException();
			data[0] = (byte)':';
			data[data.Length - 2] = (byte)'\r';
			data[data.Length - 1] = (byte)'\n';
			_offset = 0;
			_count = data.Length;
			_buffer = data;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private RespInteger(byte[] buffer, int offset, int count)
		{
			_offset = offset;
			_count = count;
			_buffer = buffer;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static implicit operator RespData(RespInteger intval)
		{
			return new RespData(intval._buffer, intval._offset, intval._count);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static explicit operator RespInteger(RespData data)
		{
			return new RespInteger(data._buffer, data._offset, data._count);
		}

		public long GetInt64()
		{
			if (_buffer == null || _count <= 3 || _buffer[_offset] != ':')
				throw new InvalidOperationException();
			return long.Parse(Encoding.ASCII.GetString(_buffer, _offset + 1, _count - 3), NumberFormatInfo.InvariantInfo);
		}

	}
}
