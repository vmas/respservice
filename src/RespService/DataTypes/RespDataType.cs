﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RespService.DataTypes
{
    public enum RespDataType
    {
		Undefined = 0,
		String = '+',
		Bulk = '$',
		Array = '*',
		Integer = ':',
		Error = '-',

    }
}
