﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

namespace RespService.DataTypes
{
	[StructLayout(LayoutKind.Explicit)]
	public struct RespData : IEquatable<RespData>
	{
		[FieldOffset(0)]
		internal readonly int _offset;
		[FieldOffset(4)]
		internal readonly int _count;
		[FieldOffset(8)]
		internal readonly byte[] _buffer;

		private const int MAX_LENGTH = 512 * 1024 * 1024;
		public static readonly RespData None;
		public static readonly RespBulk NullBulk = new RespBulk(new ArraySegment<byte>());
		public static readonly RespArray NullArray = new RespArray(null);

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public RespData(byte[] buffer, int offset, int count)
		{
			_offset = offset;
			_count = count;
			_buffer = buffer;
		}

		public ArraySegment<byte> GetBuffer()
		{
			if (_buffer == null || _offset < 0 || _offset + _count > _buffer.Length)
				throw new InvalidOperationException();
			return new ArraySegment<byte>(_buffer, _offset, _count);
		}

		public object GetValue(Encoding encoding)
		{
			if (_count >= 3)
			{
				switch (_buffer[_offset])
				{
					case (byte)'*':
						return ((RespArray)this);
					case (byte)'+':
						return ((RespString)this).GetString(encoding);
					case (byte)'$':
						return ((RespBulk)this).GetString(encoding);
					case (byte)'-':
						return new RespException(((RespError)this).GetString(encoding));
					case (byte)':':
						return ((RespInteger)this).GetInt64();
				}
			}
			throw new InvalidOperationException();
		}

		public RespDataType DataType
		{
			get
			{
				if (_buffer == null || _count < 2)
					return RespDataType.Undefined;

				switch (_buffer[_offset])
				{
					case (byte)'*':
						return RespDataType.Array;
					case (byte)'$':
						return RespDataType.Bulk;
					case (byte)'-':
						return RespDataType.Error;
					case (byte)':':
						return RespDataType.Integer;
				}
				return RespDataType.String;
			}
		}

		internal static RespData ParseStream(RespProtocolStream lineStream, long startOffset)
		{
			ArraySegment<byte> segment = ParseData(lineStream, startOffset);
			if (segment.Array == null)
				return RespData.None;
			var buffer = new byte[segment.Count];
			Buffer.BlockCopy(segment.Array, segment.Offset, buffer, 0, buffer.Length);
			return new RespData(buffer, 0, buffer.Length);
		}

		internal static ArraySegment<byte> ParseData(RespProtocolStream lineStream, long startOffset)
		{
			lineStream.Seek(startOffset, SeekOrigin.Begin);
			switch (lineStream.ReadByte())
			{
				case ':':
					return ParseInteger(lineStream, startOffset);
				case '+':
					return ParseString(lineStream, startOffset);
				case '$':
					return ParseBulk(lineStream, startOffset);
				case '*':
					return ParseMultiBulk(lineStream, startOffset);
				case '-':
					return ParseError(lineStream, startOffset);
			}
			lineStream.Seek(startOffset, SeekOrigin.Begin);
			return ParseString(lineStream, startOffset);
		}

		private static ArraySegment<byte> ParseInteger(RespProtocolStream stream, long startOffset)
		{
			stream.Seek(startOffset, SeekOrigin.Begin);
			if (stream.ReadByte() != ':')
				throw new InvalidOperationException();

			ArraySegment<byte> segment;
			if (!TryReadToLineEnd(stream, (int)startOffset, out segment))
				return new ArraySegment<byte>();

			long value;
			if (!long.TryParse(Encoding.ASCII.GetString(segment.Array, segment.Offset + 1, segment.Count - 3), out value))
				throw new RespProtocolException("invalid integer value", true);
			return segment;
		}

		private static ArraySegment<byte> ParseString(RespProtocolStream stream, long startOffset)
		{
			ArraySegment<byte> segment;
			if (!TryReadToLineEnd(stream, (int)startOffset, out segment))
				return new ArraySegment<byte>();
			return segment;
		}

		private static ArraySegment<byte> ParseError(RespProtocolStream stream, long startOffset)
		{
			stream.Seek(startOffset, SeekOrigin.Begin);
			if (stream.ReadByte() != '-')
				throw new InvalidOperationException();

			ArraySegment<byte> segment;
			if (!TryReadToLineEnd(stream, (int)startOffset, out segment))
				return new ArraySegment<byte>();
			return segment;
		}

		private static ArraySegment<byte> ParseBulk(RespProtocolStream lineStream, long startOffset)
		{
			lineStream.Seek(startOffset, SeekOrigin.Begin);
			if (lineStream.ReadByte() != '$')
				throw new InvalidOperationException();

			ArraySegment<byte> segment;
			if (!TryReadToLineEnd(lineStream, (int)startOffset, out segment))
				return new ArraySegment<byte>();

			lineStream.StatementConditionOffset = lineStream.Position;
			int length = -1;
			if (!int.TryParse(Encoding.ASCII.GetString(segment.Array, segment.Offset + 1, segment.Count - 3), out length) || length < -1 || length > MAX_LENGTH)
				throw new RespProtocolException("invalid bulk length", true);

			if (length != -1)
			{ 
				long current = lineStream.Position;
				long planned = current + length;
				if (planned + 2 > lineStream.Length)
				{
					lineStream.Seek(0, SeekOrigin.End);
					return new ArraySegment<byte>();
				}

				lineStream.Seek(planned, SeekOrigin.Begin);
				if (!TryReadToLineEnd(lineStream, (int)startOffset, out segment))
					return new ArraySegment<byte>();
			}
			return segment;
		}

		private static ArraySegment<byte> ParseMultiBulk(RespProtocolStream lineStream, long startOffset)
		{
			lineStream.Seek(startOffset, SeekOrigin.Begin);
			if (lineStream.ReadByte() != '*')
				throw new InvalidOperationException();

			ArraySegment<byte> segment;
			if (!TryReadToLineEnd(lineStream, (int)startOffset, out segment))
				return new ArraySegment<byte>();

			lineStream.StatementConditionOffset = lineStream.Position;
			int count = -1;
			if (!int.TryParse(Encoding.ASCII.GetString(segment.Array, segment.Offset + 1, segment.Count - 3), out count) || count < -1)
				throw new RespProtocolException("invalid multibulk length", true);

			int size = segment.Count;
			for (int i = 0; i < count; i++)
			{
				ArraySegment<byte> itemSegment = ParseData(lineStream, startOffset + size);
				if (itemSegment.Array == null)
					return new ArraySegment<byte>();
				size += itemSegment.Count;
			}
			
			return new ArraySegment<byte>(segment.Array, segment.Offset, size);
		}

		public static bool TryReadToLineEnd(MemoryStream stream, int startOffset, out ArraySegment<byte> segment)
		{
			int prevByte = -1;
			while (true)
			{
				int b = stream.ReadByte();
				if (b == -1)
				{
					segment = new ArraySegment<byte>();
					return false;
				}

				if (b == '\n' && prevByte == '\r')
				{
					break;
				}
				prevByte = b;
			}

#if CORECLR
			if (!stream.TryGetBuffer(out ArraySegment<byte> buffer))
				throw new UnauthorizedAccessException("RespProtocolStream's internal buffer cannot be accessed.");
#else
			var buffer = new ArraySegment<byte>(stream.GetBuffer());
#endif
			segment = new ArraySegment<byte>(buffer.Array, buffer.Offset + startOffset, (int)stream.Position - startOffset);
			return true;
		}

		public static bool operator != (RespData a, RespData b)
		{
			if (a._buffer != b._buffer)
				return true;
			return a._buffer != null && b._buffer != null && (a._offset != b._offset || a._count != b._count);
		}

		public static bool operator ==(RespData a, RespData b)
		{
			if (a._buffer != b._buffer)
				return false;
			return a._buffer == null || (a._offset == b._offset && a._count == b._count);
		}
 
		public bool Equals(RespData other)
		{
			if (other._buffer != _buffer)
				return false;
			return _buffer == null || (_offset == other._offset && _count == other._count);
		}

		public override bool Equals(object obj)
		{
			return obj is RespData && Equals((RespData)obj);
		}

		public override int GetHashCode()
		{
			return (_buffer != null ? _buffer.GetHashCode() : 0) | _offset.GetHashCode() | _count.GetHashCode();
		}

	}
}
