﻿using System;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

namespace RespService.DataTypes
{
	[StructLayout(LayoutKind.Explicit)]
	public struct RespBulk
	{
		[FieldOffset(0)]
		private readonly int _offset;
		[FieldOffset(4)]
		private readonly int _count;
		[FieldOffset(8)]
		private readonly byte[] _buffer;

		public unsafe RespBulk(ArraySegment<byte> segment)
		{
			byte[] data;
			if (segment.Array == null)
			{
				data = new byte[] { (byte)'$', (byte)'-', (byte)'1', (byte)'\r', (byte)'\n' };
			}
			else
			{
				byte[] bufferSize = Encoding.ASCII.GetBytes(segment.Count.ToString(NumberFormatInfo.InvariantInfo));
				data = new byte[segment.Count + 5 + bufferSize.Length];
				fixed (byte* ptr = data)
				{
					ptr[0] = (byte)'$';
					ptr[bufferSize.Length + 1] = (byte)'\r';
					ptr[bufferSize.Length + 2] = (byte)'\n';
					ptr[data.Length - 2] = (byte)'\r';
					ptr[data.Length - 1] = (byte)'\n';
				}
				Buffer.BlockCopy(bufferSize, 0, data, 1, bufferSize.Length);
				Buffer.BlockCopy(segment.Array, segment.Offset, data, bufferSize.Length + 3, segment.Count);
			}
			_offset = 0;
			_count = data.Length;
			_buffer = data;
		}

		public RespBulk(string s, Encoding encoding)
			: this(s != null ? new ArraySegment<byte>(encoding.GetBytes(s)) : new ArraySegment<byte>())
		{

		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private RespBulk(byte[] buffer, int offset, int count)
		{
			_offset = offset;
			_count = count;
			_buffer = buffer;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static implicit operator RespData(RespBulk bulk)
		{
			
			return new RespData(bulk._buffer, bulk._offset, bulk._count);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static explicit operator RespBulk(RespData data)
		{
			return new RespBulk(data._buffer, data._offset, data._count);
		}

		private void AssertValidBuffer()
		{
			if (_buffer == null || _offset < 0 || _count < 0 || _offset + _count > _buffer.Length || _buffer[_offset] != '$')
				throw new InvalidOperationException();
		}

		public bool IsNull()
		{
			return GetLength() == - 1;
		}

		public int GetLength()
		{
			AssertValidBuffer();

			int crPos = Array.IndexOf<byte>(_buffer, (byte)'\r', _offset, _count);
			return int.Parse(Encoding.ASCII.GetString(_buffer, _offset + 1, crPos - _offset - 1));
		}

		public string GetString(Encoding encoding)
		{
			ArraySegment<byte> segment = GetBinary();
			if (segment.Array == null)
				return null;
			return encoding.GetString(segment.Array, segment.Offset, segment.Count);
		}

		public ArraySegment<byte> GetBinary()
		{
			AssertValidBuffer();

			int crPos = Array.IndexOf<byte>(_buffer, (byte)'\r', _offset, _count);
			int size = int.Parse(Encoding.ASCII.GetString(_buffer, _offset + 1, crPos - _offset - 1));
			if (_count < crPos - _offset + 1 + size + 2)
				throw new FormatException("Invalid bulk length.");
			if (size == -1)
				return new ArraySegment<byte>();
			return new ArraySegment<byte>(_buffer, crPos + 2, size);
		}

	}
}
