﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace RespService
{
	sealed class SyncHandle : IDisposable
	{
		private Queue<TaskCompletionSource<bool>> _queue;
		private int _count;
		private int _maxCount;

		public SyncHandle(int initialCount)
			: this(initialCount, initialCount)
		{

		}

		public SyncHandle(int initialCount, int maxCount)
		{
			_count = maxCount - initialCount;
			_maxCount = maxCount;
			_queue = new Queue<TaskCompletionSource<bool>>();
		}

		public void Dispose()
		{
			Queue<TaskCompletionSource<bool>> queue = _queue;
			if (queue == null)
				return;
			lock (queue)
			{
				Interlocked.Exchange(ref _queue, null);
			}
		}

		private Queue<TaskCompletionSource<bool>> Queue
		{
			get
			{
				var queue = Volatile.Read(ref _queue);
				if (queue == null)
					throw new ObjectDisposedException(this.GetType().Name);
				return queue;
			}
		}

		public int CurrentCount
		{
			get { return Math.Max(_maxCount - Volatile.Read(ref _count), 0); }
		}

		public bool Wait(TimeSpan timeout, CancellationToken cancellationToken)
		{
			TaskCompletionSource<bool> tcs;
			lock (this.Queue)
			{
				int count = Volatile.Read(ref _count);
				if (count < _maxCount)
				{
					Volatile.Write(ref _count, count + 1);
					return true;
				}
				tcs = new TaskCompletionSource<bool>();
				_queue.Enqueue(tcs);
			}

			try
			{
				if (!tcs.Task.Wait((int)timeout.TotalMilliseconds, cancellationToken))
				{
					if (!TryCancelTask(tcs, false))
						return true;
				}
			}
			catch
			{
				TryCancelTask(tcs, true);
				throw;
			}
			return tcs.Task.GetAwaiter().GetResult();
		}

		public async Task<bool> WaitAsync(TimeSpan timeout, CancellationToken cancellationToken)
		{
			TaskCompletionSource<bool> tcs;
			lock (this.Queue)
			{
				int count = Volatile.Read(ref _count);
				if (count < _maxCount)
				{
					Volatile.Write(ref _count, count + 1);
					return true;
				}
				tcs = new TaskCompletionSource<bool>();
				_queue.Enqueue(tcs);
			}


			using (CancellationTokenSource cst = new CancellationTokenSource())
			{
				Task delayTask = Task.Delay(timeout, cst.Token);
				CancellationTokenRegistration ctr = cancellationToken.Register(CancelWaitTask, tcs);
				try
				{
					await Task.WhenAny(tcs.Task, delayTask).ConfigureAwait(false);
				}
				finally
				{
					ctr.Dispose();
					cst.Cancel();
				}
			}
			if (TryCancelTask(tcs, false))
				return false;
			return await tcs.Task.ConfigureAwait(false);
		}

		private void CancelWaitTask(object state)
		{
			Queue<TaskCompletionSource<bool>> queue = _queue;
			if (queue == null)
			{
				((TaskCompletionSource<bool>)state).TrySetCanceled();
			}
			else
			{
				lock (queue)
				{
					((TaskCompletionSource<bool>)state).TrySetCanceled();
				}
			}
		}

		private bool TryCancelTask(TaskCompletionSource<bool> tcs, bool force)
		{
			bool status;
			lock (_queue)
			{
				status = tcs.TrySetResult(false);
			}
			if (status)
				return true;
			if (!force)
				return false;

			Release();
			return false;
		}

		public int Release()
		{
			lock (this.Queue)
			{
				while (_queue.Count > 0)
				{
					if (_queue.Dequeue().TrySetResult(true))
					{
						return Volatile.Read(ref _count);
					}
				}
				return Interlocked.Decrement(ref _count);
			}
		}


	}
}
