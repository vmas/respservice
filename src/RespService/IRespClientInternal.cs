﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RespService
{
	public interface IRespClientInternal : IRespClient
    {
		void MoveToPool(RespClientPool pool);
		void MakeAvailable(object verificationToken);
		void MakeUnavailable(object verificationToken);
		bool TryMakeUnavailableThreadSafe(object verificationToken);
		TimeSpan GetIdleTime();
		bool Available { get; }
	}
}
