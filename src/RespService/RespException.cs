﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RespService
{
	public class RespException : Exception
	{

		public RespException(string message)
			: base(message)
		{

		}

		public RespException(string message, Exception innerException)
			: base(message, innerException)
		{

		}

		public override string ToString()
		{
			return this.Message;
		}
	}
}
