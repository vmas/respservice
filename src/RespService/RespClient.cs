﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace RespService
{
    public class RespClient : RespServerConnection, IRespClient, IRespClientInternal
	{
		private object _verificationToken;
		private RespClientPool _parentPool;
		private string _hostname;
		private int _port;
		private volatile bool _exceptionOcurred;
		private DateTime _idleStartTime;

		public RespClient(string hostname, int port)
		{
			_hostname = hostname;
			_port = port;
			_idleStartTime = DateTime.UtcNow;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				RespClientPool pool = Volatile.Read(ref _parentPool);
				if (pool != null)
				{
					if (!_exceptionOcurred && this.Connected)
					{
						if (pool.RevokeClient(this))
							return;
						Interlocked.Exchange(ref _parentPool, pool);
					}
					else
					{
						pool.RemoveFromPool(this);
					}
				}
			}
			
			base.Dispose(disposing);
		}

		bool IRespClientInternal.Available
		{
			get
			{
				return Volatile.Read(ref _verificationToken) == null;
			}
		}

		void IRespClientInternal.MoveToPool(RespClientPool pool)
		{
			Interlocked.Exchange(ref _parentPool, pool);
		}

		void IRespClientInternal.MakeAvailable(object verificationToken)
		{
			_idleStartTime = DateTime.UtcNow;
			if(Interlocked.CompareExchange(ref _verificationToken, null, verificationToken) != verificationToken)
				throw new InvalidOperationException();
		}

		void IRespClientInternal.MakeUnavailable(object verificationToken)
		{
			if (Interlocked.CompareExchange(ref _verificationToken, verificationToken, null) != null)
				throw new InvalidOperationException();
		}

		bool IRespClientInternal.TryMakeUnavailableThreadSafe(object verificationToken)
		{
			return Interlocked.CompareExchange(ref _verificationToken, verificationToken, null) == null;
		}

		TimeSpan IRespClientInternal.GetIdleTime()
		{
			return DateTime.UtcNow - _idleStartTime;
		}

		protected RespServerConnection Connection
		{
			get
			{
				if (_parentPool != null && Volatile.Read(ref _verificationToken) == null)
					throw new ObjectDisposedException(this.GetType().Name);
				return this;
			}
		}

		public RespClientPool Pool
		{
			get { return Volatile.Read(ref _parentPool); }
		}

		public TimeSpan Idle
		{
			get
			{
				return ((IRespClientInternal)this).Available ? DateTime.UtcNow - _idleStartTime : TimeSpan.Zero;
			}
		}

		public override bool Connected
		{
			get
			{
				if (base.Connected)
				{
					try
					{
						return (!this.Socket.Poll(0, SelectMode.SelectRead) && this.Socket.Available == 0);
					}
					catch (SocketException) { }
				}
				return false;
			}
		}

		public void Connect()
		{
			if (_hostname == null || _port <= 0)
				throw new InvalidOperationException();
			this.Connection.Connect(_hostname, _port);
		}

		public Task ConnectAsync()
		{
			if (_hostname == null || _port <= 0)
				throw new InvalidOperationException();
			return this.Connection.ConnectAsync(_hostname, _port);
		}

		protected override void ProcessException(Exception exception)
		{
			_exceptionOcurred = true;
		}

		protected override void ConfigureSocket(Socket socket)
		{
			base.ConfigureSocket(socket);
			EnableKeepAlive(socket);
		}

	}
}
