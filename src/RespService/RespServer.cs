﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace RespService
{
	public class RespServer : IDisposable
	{
		private volatile bool _exit;
		HashSet<RespConnection> activeClients = new HashSet<RespConnection>();
		private TcpListener _tcpListener;
		private SemaphoreSlim _completeEvent;
		private long _activeTasks;
		private Func<Socket, RespServer, RespClientConnection> createConnection;
		public event EventHandler<UnobservedTaskExceptionEventArgs> UnobservedException = delegate { };
		private object SyncRoot;
		private CancellationTokenSource _shutdownTokenSource;
		private Task _shutdownTask;
		private int _shutdownTimeout;

		public RespServer(IPAddress ip, int port, Func<Socket, RespServer, RespClientConnection> createConnectionCallback)
		{
			if (createConnectionCallback == null)
				throw new ArgumentNullException(nameof(createConnectionCallback));

			this.SyncRoot = new object();
			_tcpListener = new TcpListener(ip, port);
			_completeEvent = new SemaphoreSlim(1, 1);
			this.createConnection = createConnectionCallback;
			_shutdownTokenSource = new CancellationTokenSource();
			_shutdownTask = Task.Delay(Timeout.Infinite, this.ShutdownToken);
			_shutdownTimeout = 5000;
		}

		public void Dispose()
		{
			_tcpListener.Stop();
		}

		public bool IsBusy { get; private set; }

		public int Count
		{
			get
			{
				return activeClients.Count;
			}
		}

		public int ShutdownTimeout
		{
			get
			{
				return _shutdownTimeout;
			}
			set
			{
				if (value < 0 && value != Timeout.Infinite)
					throw new ArgumentOutOfRangeException("value");
				_shutdownTimeout = value;
			}
		}

		internal bool AllowConnections
		{
			get { return !_exit; }
		}

		public CancellationToken ShutdownToken
		{
			get { return _shutdownTokenSource.Token; }
		}

		public Task ShutdownTask
		{
			get { return _shutdownTask; }
		}

		public void RunCli(bool showHelp)
		{
			if (showHelp)
			{
				Console.WriteLine("Enter 'quit' to exit...");
			}
			Task.Run(() =>
			{

				while (true)
				{
					string line = Console.ReadLine();
					switch (line)
					{
						case "quit":
							Stop();
							return;
					}
				}
			}).ConfigureAwait(false);
		}

		public async Task Run()
		{
			if (this.IsBusy)
				throw new InvalidOperationException();

			_tcpListener.Server.NoDelay = true;
			_tcpListener.Server.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
			_tcpListener.Start();
			try
			{
				try
				{
					this.IsBusy = true;
					while (this.AllowConnections)
					{
						Socket clientSocket;
						try
						{
							clientSocket = await _tcpListener.AcceptSocketAsync().ConfigureAwait(false);
						}
						catch (ObjectDisposedException)
						{
							break;
						}
						RememberProcessTask(ProcessClient(clientSocket));
					}
				}
				finally
				{
					_tcpListener.Stop();
				}
				if (Interlocked.Read(ref _activeTasks) > 0)
				{
					_completeEvent.Wait();
				}
			}
			finally
			{
				this.IsBusy = false;
			}
		}

		public virtual void Stop()
		{
			_exit = true;
			_tcpListener.Stop();
			_shutdownTokenSource.CancelAfter(this.ShutdownTimeout);
		}

		private async Task ProcessClient(Socket clientSocket)
		{
			var client = createConnection(clientSocket, this);
			try
			{
				
				lock (SyncRoot)
				{
					activeClients.Add(client);
				}
				await client.ProcessAsync().ConfigureAwait(false);
			}
			catch (OperationCanceledException) { }
			catch (SocketException) { }
			catch (IOException) { }
			finally
			{
				lock (SyncRoot)
				{
					activeClients.Remove(client);
				}
				client.Dispose();
			}
		}

		private async void RememberProcessTask(Task task)
		{
			try
			{
				_completeEvent.Wait(0);
				Interlocked.Increment(ref _activeTasks);
				await task.ConfigureAwait(false);
			}
			catch (AggregateException aex) when (aex.InnerException?.InnerException is SocketException) { }
			catch (Exception ex)
			{
				var args = new UnobservedTaskExceptionEventArgs(ex as AggregateException ?? new AggregateException(ex));
				UnobservedException(this, args);
				if (!args.Observed)
					throw;
			}
			finally
			{
				long count = Interlocked.Decrement(ref _activeTasks);
				if (count == 0 && !this.AllowConnections)
				{
					_completeEvent.Release();
				}
			}
		}

	}
}
