﻿using System;
using System.Linq;
using RespService.DataTypes;
using System.Text;

namespace RespService
{
	public sealed class RespCommand
    {
		private readonly RespArray _multibulk;
		private readonly RespData[] _data;

		public RespCommand(RespArray commandData, Encoding encoding)
		{
			if (((RespData)commandData)._buffer == null)
				throw new ArgumentOutOfRangeException(nameof(commandData));
			RespData[] items = commandData.ToArray();
			if (items.Length == 0)
				throw new ArgumentOutOfRangeException(nameof(commandData));
			string commandName = ((RespBulk)items[0]).GetString(encoding);
			if (commandName == null)
				throw new ArgumentOutOfRangeException(nameof(commandData));
			this.RawName = commandName;
			this.Name = commandName.ToUpperInvariant();
			this.Encoding = encoding;
			_multibulk = commandData;
			_data = items;
		}

		public string Name { get; }

		public string RawName { get; }

		public Encoding Encoding { get; }

		public int ArgsCount
		{
			get { return _data.Length - 1; }
		}

		public RespData this[int index]
		{
			get
			{
				if (index < 0 || index >= this.ArgsCount)
					throw new RespException($"Wrong number of arguments for '{Name}' command");

				return _data[index + 1];
			}
		}


		public long GetInt64Arg(int index, string argName = "value")
		{
			object value = this[index].GetValue(this.Encoding);
			if (value is long)
				return (long)value;
			if (long.TryParse(value as string, out long v))
				return v;
			throw new RespException(argName + " is not an integer");
		}

		public int GetInt32Arg(int index, string argName = "value")
		{
			long value = GetInt64Arg(index, argName);
			if (value > int.MaxValue || value < int.MinValue)
				throw new RespException(argName + " is out of range");
			return (int)value;
			throw new RespException(argName + " is not an integer");
		}

		public string GetStringArg(int index, string argName = "value")
		{
			RespData value = this[index];
			RespDataType valueType = value.DataType;
			if (valueType == RespDataType.String)
				return ((RespString)value).GetString(this.Encoding);
			if (valueType == RespDataType.Bulk)
				return ((RespBulk)value).GetString(this.Encoding);
			throw new RespException(argName + " is not a string");
		}
		  
	}
}
