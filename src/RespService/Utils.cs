﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace RespService
{
	internal static class Utils
	{

#if !CORECLR

		public static readonly bool IsMono = (Type.GetType("Mono.Runtime") != null);

		public static async Task ConnectAsync(this Socket socket, EndPoint endPoint)
		{
			var handler = new EventHandler<SocketAsyncEventArgs>((s, e) =>
			{
				var taskSource = e.UserToken as TaskCompletionSource<bool>;
				if (e.ConnectByNameError != null)
					taskSource.SetException(e.ConnectByNameError);
				else if (e.SocketError != SocketError.Success)
					taskSource.SetException(new SocketException((int)e.SocketError));
				else
					taskSource.SetResult(true);
			});

			using (var ea = new SocketAsyncEventArgs())
			{
				ea.UserToken = new TaskCompletionSource<bool>();
				ea.RemoteEndPoint = endPoint;
				ea.Completed += handler;
				try
				{
					if (socket.ConnectAsync(ea))
					{
						await ((TaskCompletionSource<bool>)(ea.UserToken)).Task.ConfigureAwait(false);
					}
					else if (ea.ConnectByNameError != null)
					{
						throw ea.ConnectByNameError;
					}
					else if (ea.SocketError != SocketError.Success)
					{
						throw new SocketException((int)ea.SocketError);
					}
				}
				finally
				{
					ea.Completed -= handler;
				}
			}
		}

#endif

	}
}
