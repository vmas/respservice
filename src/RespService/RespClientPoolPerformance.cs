﻿using System.Linq;
using System.Threading;

namespace RespService
{
    public sealed class RespClientPoolPerformance
    {
		private RespClientPool _pool;
		internal int _starvation;
		internal int _waiting;
		internal int _connecting;

		internal RespClientPoolPerformance(RespClientPool pool)
		{
			_pool = pool;
		}

		public int Starvation
		{
			get { return Volatile.Read(ref _starvation); }
		}

		public int Available
		{
			get { return _pool.Available; }
		}

		public int UnusedSlots
		{
			get { return _pool.Clients.Where(client => client == null).Count(); }
		}

		public int Inactive
		{
			get { return _pool.Clients.Where(client => client != null && client.Available).Count(); }
		}

		public int Waiting
		{
			get { return Volatile.Read(ref _waiting); }
		}
		
		public int Connecting
		{
			get { return Volatile.Read(ref _connecting); }
		} 

		public override string ToString()
		{
			return $"a: {Available}; w: {Waiting}; c: {Connecting}; u: {UnusedSlots}; s: {Starvation}, i: {Inactive}";
		}

	}
}
