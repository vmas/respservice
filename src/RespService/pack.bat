@echo off
set VERSION_SUFFIX=%1
:check_version_suffix
if "%VERSION_SUFFIX%"=="" (
    set /P VERSION_SUFFIX="Enter version suffix: "
    goto check_version_suffix
    exit /b 1
)
dotnet build -c Release --version-suffix %VERSION_SUFFIX% --no-incremental
dotnet pack -c Release --version-suffix %VERSION_SUFFIX% 