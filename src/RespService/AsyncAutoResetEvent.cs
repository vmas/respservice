﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RespService
{
    sealed class AsyncAutoResetEvent : IDisposable
    {
		private readonly Queue<TaskCompletionSource<bool>> m_waits = new Queue<TaskCompletionSource<bool>>();
		private readonly AutoResetEvent _syncEvent;
		private volatile bool m_signaled;

		public AsyncAutoResetEvent()
		{
			_syncEvent = new AutoResetEvent(false);
		}

		~AsyncAutoResetEvent()
		{
			Dispose();
		}

		public void Dispose()
		{
			lock (m_waits)
			{
				m_waits.Clear();
			}
			_syncEvent.Dispose();
		}

		public bool Wait(TimeSpan timeout)
		{
			return _syncEvent.WaitOne(timeout);
		}

		public Task WaitAsync()
		{
			lock (m_waits)
			{
				if (m_signaled)
				{
					m_signaled = false;
#if CORECLR
					return Task.CompletedTask;
#else
					return Task.Delay(0);
#endif
				}
				else
				{
					var tcs = new TaskCompletionSource<bool>();
					m_waits.Enqueue(tcs);
					return tcs.Task;
				}
			}
		}

		public void Set()
		{
			TaskCompletionSource<bool> toRelease = null;
			lock (m_waits)
			{
				if (m_waits.Count > 0)
					toRelease = m_waits.Dequeue();
				else if (!m_signaled)
					m_signaled = true;
			}
			if (toRelease != null)
				toRelease.SetResult(true);
			_syncEvent.Set();
		}

	}
}
