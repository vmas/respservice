﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RespService
{
	public interface IRespClientFactory
    {
		IRespClientInternal CreateClient();
    }
}
