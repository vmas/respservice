﻿using RespService.DataTypes;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace RespService
{
	public class RespServerConnection : RespConnection
	{
		private static readonly CancellationTokenSource _cancellationTokenSource;
		private static readonly Task _shutdownTask;
		private bool _isDisposed;

		static RespServerConnection()
		{
			_cancellationTokenSource = new CancellationTokenSource();
			_shutdownTask = Task.Delay(Timeout.Infinite, _cancellationTokenSource.Token);
		}

		public RespServerConnection()
		{

		}

		protected override void Dispose(bool disposing)
		{
			_isDisposed = true;
			base.Dispose(disposing);
		}

		private void ThrowIfDisposed()
		{
			if (_isDisposed)
				throw new ObjectDisposedException(this.GetType().Name);
		}

		protected override Task ShutdownTask
		{
			get
			{
				return _shutdownTask;
			}
		}

		protected override CancellationToken ShutdownToken
		{
			get
			{
				return _cancellationTokenSource.Token;
			}
		}

		protected override void ThrowInassignedSockedException()
		{
			if (_isDisposed)
				throw new ObjectDisposedException(this.GetType().Name);
			throw new InvalidOperationException("The connection has not been established.");
		}

		protected override void ConfigureSocket(Socket socket)
		{
			socket.NoDelay = true;
		}
 
		protected async Task ConnectSocketAsync(DnsEndPoint endPoint)
		{
			ThrowIfDisposed();

			if (this.Connected)
				throw new SocketException((int)SocketError.IsConnected);

			Exception exception = null;
			IPAddress[] addresses = ("." == endPoint.Host ? new[] { IPAddress.Loopback } : await Dns.GetHostAddressesAsync(endPoint.Host).ConfigureAwait(false));
			foreach (IPAddress address in addresses)
			{
				ReleaseSocket();
				AssignSocket(new Socket(address.AddressFamily, SocketType.Stream, ProtocolType.Tcp));
				try
				{
					await base.Socket.ConnectAsync(new IPEndPoint(address, endPoint.Port)).ConfigureAwait(false);
					return;
				}
				catch (Exception e)
				{
					exception = e;
				}
			}
			if (exception != null)
				throw exception;
			throw new SocketException((int)SocketError.HostNotFound);
		}

		protected void ConnectSocket(DnsEndPoint endPoint)
		{
			ThrowIfDisposed();

			if (this.Connected)
				throw new SocketException((int)SocketError.IsConnected);

			Exception exception = null;
			IPAddress[] addresses = ("." == endPoint.Host ? new[] { IPAddress.Loopback } : Dns.GetHostAddressesAsync(endPoint.Host).Result);
			foreach (IPAddress address in addresses)
			{
				ReleaseSocket();
				AssignSocket(new Socket(address.AddressFamily, SocketType.Stream, ProtocolType.Tcp));
				try
				{
					base.Socket.Connect(address, endPoint.Port);
					return;
				}
				catch (Exception e)
				{
					exception = e;
				}
			}

			if (exception != null)
				throw exception;
			throw new SocketException((int)SocketError.HostNotFound);
		}

		public Task ConnectAsync(EndPoint remoteEndPoint)
		{
			ThrowIfDisposed();

			if (this.Connected)
				throw new SocketException((int)SocketError.IsConnected);

#if CORECLR
			if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
			{
				ReleaseSocket();
				AssignSocket(new Socket(SocketType.Stream, ProtocolType.Tcp));
				return base.Socket.ConnectAsync(remoteEndPoint);
			}
#else
			if (!Utils.IsMono)
			{
				ReleaseSocket();
				AssignSocket(new Socket(SocketType.Stream, ProtocolType.Tcp));
				return base.Socket.ConnectAsync(remoteEndPoint);
			}
#endif
			if (remoteEndPoint is IPEndPoint ipEndPoint)
			{
				ReleaseSocket();
				AssignSocket(new Socket(ipEndPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp));
				return base.Socket.ConnectAsync(ipEndPoint);
			}

			var dnsEndPoint = remoteEndPoint as DnsEndPoint;
			if (dnsEndPoint == null)
				throw new NotSupportedException("This type of endpoint not supported.");
			return ConnectSocketAsync(dnsEndPoint);
		}

		public void Connect(EndPoint remoteEndPoint)
		{
			ThrowIfDisposed();

			if (this.Connected)
				throw new SocketException((int)SocketError.IsConnected);

#if CORECLR
			if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
			{
				ReleaseSocket();
				AssignSocket(new Socket(SocketType.Stream, ProtocolType.Tcp));
				base.Socket.Connect(remoteEndPoint);
				return;
			}
#else
			if (!Utils.IsMono)
			{
				ReleaseSocket();
				AssignSocket(new Socket(SocketType.Stream, ProtocolType.Tcp));
				base.Socket.Connect(remoteEndPoint);
				return;
			}
#endif
			if (remoteEndPoint is IPEndPoint ipEndPoint)
			{
				ReleaseSocket();
				AssignSocket(new Socket(ipEndPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp));
				base.Socket.Connect(ipEndPoint);
				return;
			}

			var dnsEndPoint = remoteEndPoint as DnsEndPoint;
			if (dnsEndPoint == null)
				throw new NotSupportedException("This type of endpoint not supported.");
			ConnectSocket(dnsEndPoint);
		}

		public Task ConnectAsync(string host, int port)
		{
			ThrowIfDisposed();

			if (IPAddress.TryParse(host, out IPAddress address))
			{
				return ConnectAsync(new IPEndPoint(address, port));
			}
			return ConnectAsync(new DnsEndPoint(host, port));
		}

		public void Connect(string host, int port)
		{
			ThrowIfDisposed();

			if (IPAddress.TryParse(host, out IPAddress address))
			{
				Connect(new IPEndPoint(address, port));
			}
			else
			{
				Connect(new DnsEndPoint(host, port));
			}
		}

		public virtual RespData SendQuery(RespArray command)
		{
			try
			{
				WriteStatement(command);
				return ReadStatement();
			}
			catch (Exception ex)
			{
				ProcessException(ex);
				throw;
			}
		}

		public virtual async Task<RespData> SendQueryAsync(RespArray command)
		{
			try
			{
				await WriteStatementAsync(command).ConfigureAwait(false);
				return await ReadStatementAsync().ConfigureAwait(false);
			}
			catch (Exception ex)
			{
				ProcessException(ex);
				throw;
			}
		}

		public virtual async Task<RespData> SendQueryAsync(string query)
		{
			if (query == null)
				throw new ArgumentNullException(nameof(query));

			byte[] buffer = ArrayPool<byte>.Shared.Rent(this.Encoding.GetMaxByteCount(query.Length) + 2);
			int count = this.Encoding.GetBytes(query, 0, query.Length, buffer, 0);
			buffer[count++] = (byte)'\r';
			buffer[count++] = (byte)'\n';
			try
			{
				await WriteAsync(buffer, 0, count).ConfigureAwait(false);
				ArrayPool<byte>.Shared.Return(buffer);
				buffer = null;
				return await ReadStatementAsync().ConfigureAwait(false);
			}
			catch (Exception ex)
			{
				ProcessException(ex);
				throw;
			}
			finally
			{
				if(buffer != null)
					ArrayPool<byte>.Shared.Return(buffer);
			}
		}

		public virtual RespData SendQuery(string query)
		{
			if (query == null)
				throw new ArgumentNullException(nameof(query));

			byte[] buffer = ArrayPool<byte>.Shared.Rent(this.Encoding.GetMaxByteCount(query.Length) + 2);
			int count = this.Encoding.GetBytes(query, 0, query.Length, buffer, 0);
			buffer[count++] = (byte)'\r';
			buffer[count++] = (byte)'\n';
			try
			{
				Write(buffer, 0, count);
				ArrayPool<byte>.Shared.Return(buffer);
				buffer = null;
				return ReadStatement();
			}
			catch (Exception ex)
			{
				ProcessException(ex);
				throw;
			}
			finally
			{
				if (buffer != null)
					ArrayPool<byte>.Shared.Return(buffer);
			}
		}

		protected virtual void ProcessException(Exception exception)
		{

		}
		
	}
}
