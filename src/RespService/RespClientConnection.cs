﻿

using RespService.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace RespService
{
    public class RespClientConnection : RespConnection
    {
		public static readonly Task<RespData> Ok = Task.FromResult<RespData>(new RespString("OK", Encoding.UTF8));
		public static readonly Task<RespData> None = Task.FromResult<RespData>(RespData.None);
		public static readonly Task<RespData> Pong = Task.FromResult<RespData>(new RespString("PONG", Encoding.UTF8));

		private RespServer _server;
		private CancellationTokenRegistration _shutdownTokenRegistration;

		public RespClientConnection(Socket clientSocket, RespServer server)
			: base(clientSocket)
		{
			if (server == null)
				throw new ArgumentNullException(nameof(server));

			_server = server;
			this.CanRead = true;
			_shutdownTokenRegistration = server.ShutdownToken.Register(this.Shutdown, false);
		}

		protected virtual void Shutdown()
		{
			this.Socket.Shutdown(SocketShutdown.Both);
		}

		protected override void Dispose(bool disposing)
		{
			_shutdownTokenRegistration.Dispose();
			base.Dispose(disposing);
		}

		protected override void ThrowInassignedSockedException()
		{
			throw new ObjectDisposedException(this.GetType().Name);
		}

		public RespServer Server
		{
			get { return _server; }
		}

		protected override CancellationToken ShutdownToken
		{
			get
			{
				return _server.ShutdownToken;
			}
		}

		protected override Task ShutdownTask
		{
			get
			{
				return _server.ShutdownTask;
			}
		}

		protected bool CanRead { get; set; }

		public async Task ProcessAsync()
		{
			while (this.CanRead && _server.AllowConnections)
			{
				RespData data = await ReadStatementAsync().ConfigureAwait(false);

				try
				{
					data = await ProcessStatementAsync(data).ConfigureAwait(false);
				}
				catch (RespException ex)
				{
					await ProcessExceptionAsync(ex).ConfigureAwait(false);
					data = RespData.None;
				}

				if (data == RespData.None)
					continue;

				await WriteStatementAsync(data).ConfigureAwait(false);
			}
		}

		protected override Task ProcessExceptionAsync(RespException exception)
		{
			return WriteStatementAsync(new RespError("ERR " + exception.ToString(), this.Encoding));
		}


		protected async Task<RespData> ProcessStatementAsync(RespData request)
		{
			// Note: this function should be an awaitable to catch unhandled exceptions.
			try
			{
				RespDataType dataType = request.DataType;
				if (dataType == RespDataType.String)
				{
					string raw = ((RespString)request).GetString(this.Encoding);
					if (string.IsNullOrWhiteSpace(raw))
						return RespData.None;
					request = new CommandLineParser(raw).ParseToMultiBulk(this.Encoding);
					dataType = RespDataType.Array;
				}

				if (dataType != RespDataType.Array)
				{
					ArraySegment<byte> segment = request.GetBuffer();
					string statementString = this.Encoding.GetString(segment.Array, segment.Offset, segment.Count);
					throw new RespCommandException(statementString.Remove(statementString.Length - 2));
				}

				var multibulk = (RespArray)request;
				if (multibulk.IsNull())
					throw new RespCommandException("null");

				return await ExecuteCommandAsync(new RespCommand(multibulk, this.Encoding));
			}
			catch (RespException)
			{
				throw;
			}
			catch (Exception ex)
			{
				throw new RespException($"Server error [{ex.GetType().Name}]: {ex.Message}", ex);
			}
		}

		protected virtual Task<RespData> ExecuteCommandAsync(RespCommand command)
		{
			if (string.IsNullOrWhiteSpace(command.Name))
				return None;

			switch (command.Name)
			{
				case "QUIT":
					this.CanRead = false;
					return Ok;
				case "SHUTDOWN":
					this.Server.Stop();
					return Ok;
				case "PING":
					return Pong;
			}
			throw new RespCommandException(command.RawName);
		}

	}
}
