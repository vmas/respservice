﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RespService
{
	public sealed class RespProtocolException : RespException
	{

		public RespProtocolException(string message)
			: this(message, false)
		{

		}

		public RespProtocolException(string message, bool critical)
			: base(message)
		{
			this.Critical = critical;
		}

		public bool Critical { get; private set; }

		public override string ToString()
		{
			return "Protocol error: " + this.Message;
		}
	}
}
