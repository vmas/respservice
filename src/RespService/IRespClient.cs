﻿using RespService.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RespService
{
    public interface IRespClient : IDisposable
    {
		bool Connected { get; }
		void Connect();
		Task ConnectAsync();
		RespData SendQuery(string query);
		Task<RespData> SendQueryAsync(string query);
		RespClientPool Pool { get; }
	}
}
