﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RespService
{
	public sealed class RespCommandException : RespException
	{
		public RespCommandException(string message)
			: base(message)
		{

		}

		public override string ToString()
		{
			return string.Format("Unknown command: '{0}'", this.Message);
		}
	}
}
