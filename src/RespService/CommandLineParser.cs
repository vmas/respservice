﻿using System;
using System.Buffers;
using System.Collections.Generic;
using System.Text;
using RespService.DataTypes;
using System.Globalization;

namespace RespService
{
	public sealed class CommandLineParser
	{
		private struct Segment
		{
			public readonly int Offset;
			public readonly int Count;

			public Segment(int offset, int count)
			{
				Offset = offset;
				Count = count;
			}

			public override string ToString()
			{
				return Offset.ToString() + ":" + Count.ToString();
			}
		}

		private static Dictionary<char, char> DefaultEscapes = new Dictionary<char, char>
		{
			{ 't', '\t' },
			{ 'b', '\b' },
			{ '"', '"' },
			{ '\\', '\\' },
			{ 'n', '\n' },
			{ 'r', '\r' },
			{ 'a', '\a' },
		};

		private string _input;
		private IDictionary<char, char> _escapes;

		public CommandLineParser(string input)
			: this(input, DefaultEscapes)
		{

		}

		public CommandLineParser(string input, IDictionary<char, char> escapes)
		{
			if (input == null)
				throw new ArgumentNullException(nameof(input));
			if (escapes == null)
				throw new ArgumentNullException(nameof(escapes));

			_input = input;
			_escapes = escapes;
		}

		public unsafe RespArray ParseToMultiBulk(Encoding encoding)
		{
			if (encoding == null)
				throw new ArgumentNullException(nameof(encoding));

			string dataSize;
			int count = GetMaxCount();
			int prefixOffset = count.ToString(NumberFormatInfo.InvariantInfo).Length + 3;
			int bytesCount = encoding.GetByteCount(_input);
			byte[] buffer = new byte[prefixOffset + bytesCount + count * (4 + bytesCount.ToString(NumberFormatInfo.InvariantInfo).Length) + 1];

			count = 0;
			int offset = prefixOffset;
			foreach(Segment segment in SplitData())
			{
				count++;
				char[] chars = ArrayPool<char>.Shared.Rent(segment.Count);
				try
				{
					int length = Unescape(segment, chars);
					int countOfSize = encoding.GetByteCount(chars, 0, length).ToString(NumberFormatInfo.InvariantInfo).Length; // ASCII is 1-byte encoding
					length = encoding.GetBytes(chars, 0, length, buffer, offset + countOfSize + 3);
					dataSize = length.ToString(NumberFormatInfo.InvariantInfo);
					int realCountOfSize = Encoding.ASCII.GetBytes(dataSize, 0, dataSize.Length, buffer, offset + 1);
					if (realCountOfSize > countOfSize)
						throw new InvalidOperationException();

					fixed (byte* buf = &buffer[offset])
					{
						*buf = (byte)'$';
						byte* dst = buf + realCountOfSize + 1;
						*dst++ = (byte)'\r';
						*dst++ = (byte)'\n';
						if (realCountOfSize < countOfSize)
						{
							byte* src = buf + countOfSize + 3;
							byte* stop = dst + length;
							while (dst != stop)
							{
								*dst++ = *src++;
							}
						}
						else
						{
							dst = dst + length;
						}
						*dst++ = (byte)'\r';
						*dst++ = (byte)'\n';
					}
					offset += realCountOfSize + length + 5;
				}
				finally
				{
					ArrayPool<char>.Shared.Return(chars);
				}
			}
			dataSize = count.ToString(NumberFormatInfo.InvariantInfo);
			prefixOffset = prefixOffset - dataSize.Length - 2;
			count = Encoding.ASCII.GetBytes(dataSize, 0, dataSize.Length, buffer, prefixOffset);
			if (count != dataSize.Length)
				throw new InvalidOperationException();

			var data = new RespData(buffer, prefixOffset - 1, offset - prefixOffset + 1);
			fixed (byte* buf = &buffer[data._offset])
			{
				*buf = (byte)'*';
				*(buf + count + 1) = (byte)'\r';
				*(buf + count + 2) = (byte)'\n';
			}
			return (RespArray)data;
		}

		public IEnumerable<string> Parse()
		{
			foreach(Segment segment in SplitData())
			{
				string s;
				char[] chars = ArrayPool<char>.Shared.Rent(segment.Count);
				try
				{
					s = new string(chars, 0, Unescape(segment, chars));
				}
				finally
				{
					ArrayPool<char>.Shared.Return(chars);
				}
				yield return s;
			}
		}

		public unsafe int GetMaxCount()
		{
			int count = 1;
			bool skip = true;
			fixed (char* src = _input)
			{
				char* ptr = src + _input.Length;
				while (--ptr != src)
				{
					bool space = char.IsWhiteSpace(*ptr);
					if (space && !skip)
						count++;
					skip = space; // skip while two or more spaces
				}
			}
			return count;
		}

		private IEnumerable<Segment> SplitData()
		{
			bool quoted = false;
			char prev = default(char);
			int startIndex = 0;
			int endIndex = 0;
			for (int i = 0; i < _input.Length; i++)
			{
				char ch = _input[i];
				if (char.IsWhiteSpace(ch) && !quoted)
				{
					if (startIndex < endIndex)
					{
						yield return new Segment(startIndex, endIndex - startIndex);
					}
					endIndex++;
					startIndex = endIndex;
					prev = ch;
					continue;
				}
				else if (ch == '"')
				{
					if (quoted)
					{
						if (prev != '\\')
							quoted = !quoted;
					}
					else
					{
						quoted = true;
					}
				}
				endIndex++;
				prev = ch;
			}

			if (startIndex < _input.Length)
				yield return new Segment(startIndex, _input.Length - startIndex);
		}

		private unsafe int Unescape(Segment segment, char[] buffer)
		{
			bool quoted = false;
			bool escaped = false;
			fixed (char* input = _input)
			fixed (char* output = &buffer[0])
			{
				char* src = input + segment.Offset;
				char* dst = output;
				for (int i = 0; i < segment.Count; i++)
				{
					if (quoted)
					{
						if (escaped)
						{
							escaped = false;
							if (_escapes.TryGetValue(*src, out char ch))
							{
								src++;
								*dst++ = ch;
								continue;
							}
						}
						else
						{
							if (*src == '\\')
							{
								escaped = true;
								src++;
								continue;
							}
							else if (*src == '"')
							{
								quoted = false;
								src++;
								continue;
							}
						}
					}
					else if (*src == '"')
					{
						src++;
						quoted = true;
						continue;
					}
					*dst++ = *src++;
				}
				return (int)(dst - output);
			}
		}

	}
}
