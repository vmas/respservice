﻿using System;
using System.Collections.Generic;
using System.Text;
using RespService;

namespace RespClientApp
{
	sealed class RespClientFactory : IRespClientFactory
	{
		public RespClientFactory(string host, int port)
		{
			this.Host = host;
			this.Port = port;
		}

		public string Host { get; }
		public int Port { get; }


		public IRespClientInternal CreateClient()
		{
			return new MyRespClient(this.Host, this.Port);
		}
	}
}
