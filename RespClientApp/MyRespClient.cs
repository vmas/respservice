﻿using RespService;
using RespService.DataTypes;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RespClientApp
{
	sealed class MyRespClient : RespClient
	{
		public MyRespClient(string hostname, int port)
			: base(hostname, port)
		{

		}

		public string Ping()
		{
			RespData result = base.SendQuery("PING");
			if (result.DataType == RespDataType.String)
				return ((RespString)result).GetString(this.Encoding);
			if (result.DataType == RespDataType.Error)
				throw new Exception("Server exception: " + ((RespError)result).GetString(this.Encoding));
			throw new NotImplementedException();
		}

		public async Task<string> PingAsync()
		{
			RespData result = await base.SendQueryAsync("PING");
			if (result.DataType == RespDataType.String)
				return ((RespString)result).GetString(this.Encoding);
			if (result.DataType == RespDataType.Error)
				throw new Exception("Server exception: " + ((RespError)result).GetString(this.Encoding));
			throw new NotImplementedException();
		}

	}
}
