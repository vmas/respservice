﻿using System;
using System.Linq;
using RespService;
using System.Threading;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Text;

namespace RespClientApp
{
	static class Program
	{
		private static int _count;
		private static int _threadsCount;
		private static volatile bool _exit;

		static void Main(string[] args)
		{
			Console.OutputEncoding = Encoding.UTF8;

			using (var pool = new RespClientPool(100, new RespClientFactory("localhost", 6380)))
			{
				var threas = new List<Thread>();
				string command = null;
				while (command == null)
				{
					command = Console.ReadLine().ToUpperInvariant().Trim();
					if (command == "BENCHMARK")
					{
						for (var i = 0; i < 100; i++)
						{
							var t = new Thread(DoWork);
							t.IsBackground = true;
							t.Start(pool);
							threas.Add(t);
						}
						break;
					}
					else if (command == "PING")
					{
						var stopwatch = new Stopwatch();
						stopwatch.Start();
						try
						{
							PingAsync(pool).Wait(); 
							Ping(pool);
						}
						catch (Exception e)
						{
							while (e is AggregateException ex && ex.InnerException != null)
							{
								e = ex.InnerException;
							}
							Console.WriteLine("{0}: {1}", e.GetType().Name, e.Message);
						}
						finally
						{
							stopwatch.Stop();
						}
						Console.WriteLine("Elapsed: {0} ms", stopwatch.ElapsedMilliseconds);
						Console.WriteLine();
						Console.WriteLine("Press any key to exit...");
						Console.ReadKey();
						return;
					}

				}

				using (var ev = new ManualResetEventSlim(false))
				{
					Console.Clear();
					Console.Write("Count: ");
					Console.WriteLine();
					Console.WriteLine("Press any key to exit...");

					var statusThread = new Thread(DoStatusWork);
					statusThread.IsBackground = true;
					statusThread.Start(ev);

					Console.ReadKey();
					_exit = true;
					while (Volatile.Read(ref _threadsCount) > 0)
					{
						Thread.Sleep(100);
					}
					ev.Wait();
				}
				GC.KeepAlive(threas);
			}
		}

		private static void DoStatusWork(object exitEventObj)
		{
			int left = Console.CursorLeft;
			int top = Console.CursorTop;
			var exitEvent = exitEventObj as ManualResetEventSlim;
			while (!_exit)
			{
				Console.SetCursorPosition(7, 0);
				Console.WriteLine(Volatile.Read(ref _count));
				Console.SetCursorPosition(left, top);
				Thread.Sleep(30);
			}
			exitEvent.Set();
		}

		private static void DoWork(object poolObj)
		{
			Interlocked.Increment(ref _threadsCount);
			try
			{
				while (!_exit)
				{
					PingAsync((RespClientPool)poolObj).Wait();
					Interlocked.Increment(ref _count);
				}
			}
			finally
			{
				Interlocked.Decrement(ref _threadsCount);
			}
		}

		private static async Task PingAsync(RespClientPool pool)
		{
			using (var client = (MyRespClient)(await pool.GetClientAsync()))
			{
				if (await client.PingAsync() != "PONG")
					throw new InvalidProgramException();
			}
		}

		private static void Ping(RespClientPool pool)
		{
			using (var client = (MyRespClient)pool.GetClient())
			{
				if (client.Ping() != "PONG")
					throw new InvalidProgramException();
			}
		}

	}
}