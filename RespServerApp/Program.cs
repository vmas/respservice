﻿using RespService;
using System;
using System.Net;
using System.Net.Sockets;

namespace RespServerApp
{
	static class Program
	{
		static void Main(string[] args)
		{
			using (var server = new RespServer(IPAddress.Parse("127.0.0.1"), 6380, CreateRespServiceConnection))
			{
				server.RunCli(true);
				server.Run().Wait();
			}
		}

		private static RespClientConnection CreateRespServiceConnection(Socket clientSocket, RespServer server)
		{
			return new RemoteRespClient(clientSocket, server);
		}

	}

}