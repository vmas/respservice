﻿using RespService;
using RespService.DataTypes;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace RespServerApp
{
	sealed class RemoteRespClient : RespClientConnection
	{
		private readonly Dictionary<string, Func<RespCommand, Task<RespData>>> _commands;

		public RemoteRespClient(Socket clientSocket, RespServer server)
			: base(clientSocket, server)
		{
			_commands = new Dictionary<string, Func<RespCommand, Task<RespData>>>
			{
				["HELLO"] = HelloAsync,
				["CC"] = GetClientsCountAsync
			};
		}

		protected override async Task<RespData> ExecuteCommandAsync(RespCommand command)
		{
			if (command == null || string.IsNullOrWhiteSpace(command.Name))
				return await None;

			Func<RespCommand, Task<RespData>> func;
			if (_commands.TryGetValue(command.Name, out func))
			{
				try
				{
					// TODO: create per request variables
					return await func(command);
				}
				finally
				{
					// TODO: release per request variables
				}
			}

			return await base.ExecuteCommandAsync(command);
		}


		private async Task<RespData> HelloAsync(RespCommand command)
		{
			await Task.Yield();

			string name = command.GetStringArg(0, "name");
			return new RespString($"Hello, {name}!", this.Encoding);	
		}

		private async Task<RespData> GetClientsCountAsync(RespCommand command)
		{
			await Task.Yield();

			return new RespString($"Active {Server.Count} connections.", this.Encoding);
		}


	}
}
